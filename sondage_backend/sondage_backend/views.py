""" Cornice services.
"""
from cornice import Service

#Hello = service déclaré comme un service cornice
#Répond au chemin "/"
hello = Service(name='hello', path='/', description="Simplest app")

#Répond en envoyant Hello:world
#Décorateur : prend la fonction en argument, retourne la fonction, fais des trucs entre temps
@hello.get()
def get_info(request):
    """Returns Hello in JSON."""
    return {'Hello': 'World'}

superService = Service(name="super", path='/super-service', description="Who is SuperService")
@superService.get()
def get_superService(request):
	return {'Clark': 'Kent'}

@hello.post()
def post_info(request):
